function cutstr(str, len, code, unlimit) {
    var tpChar = "";
    var tplen = 0;
    for (var i = 0; i < str.length; i++) {
        tplen++;
        if (str.charCodeAt(i) > 127) tplen++;
        if (!unlimit) {
            if (tplen >= len) {
                return tpChar + code.toString();
            }
            tpChar += str.charAt(i);
        } else {
            tpChar += str.charAt(i);
            if (tplen >= len) {
                return tpChar + code.toString();
            }
        }
    }
    return str;
}
exports.cutstr=cutstr;
