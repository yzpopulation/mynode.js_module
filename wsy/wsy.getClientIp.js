/**
 * Created by admin on 2016/6/5.
 */
function getClientIp(req) {
        var ips=
            req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;
        var ipss=ips.split(':');
        return ipss[ipss.length-1];
    }
exports.getClientIp=getClientIp;
